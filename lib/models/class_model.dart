

import 'dart:math';

List<String> carNames=["Mercedes 7 ","Mercedes 8 ","Mercedes 9 ","Mercedes 5 "];
List<String> carmodel=["2015 model","2016 model","2017 model","2018 model"];
List<String> carPrice=["\$12.999","\$13.999","\$14.999","\$15.999"];
List<String> carImage=["assets/images/benz_car.png","assets/images/benz_car_02.png","assets/images/benz_car_03.png","assets/images/benz_car_04.png"];
String headline="Instinctive Techology";
List<String> des=["BMW’s flagship car, the 7 Series is available with petrol and diesel engine. Interestingly, for the first time in India, the new BMW 7 Series is available in a plug-in-hybrid option - the BMW 745Le xDrive - which combines benefits of an electric motor and a petrol engine. Apart from this, the 730Ld (diesel) is available in three trims – Design Pure Excellence, Design Pure Excellence Signature and the M Sport.",
  "BMW’s flagship car, the 7 Series is available with petrol and diesel engine. Interestingly, for the first time in India, the new BMW 7 Series is available in a plug-in-hybrid option - the BMW 745Le xDrive - which combines benefits of an electric motor and a petrol engine. Apart from this, the 730Ld (diesel) is available in three trims – Design Pure Excellence, Design Pure Excellence Signature and the M Sport..",
  "BMW’s flagship car, the 7 Series is available with petrol and diesel engine. Interestingly, for the first time in India, the new BMW 7 Series is available in a plug-in-hybrid option - the BMW 745Le xDrive - which combines benefits of an electric motor and a petrol engine. Apart from this, the 730Ld (diesel) is available in three trims – Design Pure Excellence, Design Pure Excellence Signature and the M Sport...",
  "BMW’s flagship car, the 7 Series is available with petrol and diesel engine. Interestingly, for the first time in India, the new BMW 7 Series is available in a plug-in-hybrid option - the BMW 745Le xDrive - which combines benefits of an electric motor and a petrol engine. Apart from this, the 730Ld (diesel) is available in three trims – Design Pure Excellence, Design Pure Excellence Signature and the M Sport...."
];




class CarModel
{
String cName="";
String cModel="";
String cPrice="";
String cImage="";
String cHeadline="";
String cDes="";
static CarModel getObject()
   {
     CarModel carModel=CarModel();
    Random r=Random();
   int a= r.nextInt(3);
    carModel.cName=carNames[a];
     carModel.cModel=carmodel[a];
     carModel.cPrice=carPrice[a];
     carModel.cImage=carImage[a];
     carModel.cHeadline=headline;
     carModel.cDes=des[r.nextInt(3)];
     return carModel;
   }
}
import 'package:car_rental_animation/screens/car_details_page.dart';
import 'package:car_rental_animation/models/class_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle style = Theme.of(context).textTheme.headline6!.copyWith(
        fontSize: 25,
        fontWeight: FontWeight.normal,
        color: Colors.white,
        letterSpacing: 1);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            "Car Rental",
            style: GoogleFonts.adamina(
                fontSize: 22, color: Colors.black87, letterSpacing: 1),
          ),
          centerTitle: true,
        ),
        body: Center(child:carCard(context, CarModel.getObject())));
  }

  Widget carCard(BuildContext context, CarModel model) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          PageRouteBuilder(
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) {
              return FadeTransition(
                  opacity: animation, child: CarDetailsPage(model: model,));
            },
            opaque: true,
            // transitionsBuilder: (BuildContext context,
            //     Animation<double> animation,
            //     Animation<double> secondaryAnimation,
            //     Widget child) {
            //   return child,
            // },
            transitionDuration: Duration(milliseconds: 500),
            reverseTransitionDuration: Duration(milliseconds: 1000),
          ),
        );
      },
      child: Container(
        color: Colors.white,
        width: double.infinity,
        height: 500,
        alignment: Alignment.topCenter,
        padding: EdgeInsets.only(left: 30),
        child: Container(
          width: double.infinity,
          color: Color(0xFFcf7a53),
          padding: EdgeInsets.only(left: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 20,
              ),
              Hero(
                tag: model.cName,

                child: Container(

                  width: double.infinity,
                  child: Text(
                    model.cName,
                    style: Theme.of(context).textTheme.headline6!.copyWith(
                      fontSize: 25,
                      fontWeight: FontWeight.normal,
                      color: Colors.white,
                      letterSpacing: 1),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Hero(
                tag: model.cModel,

                child: Container(

                  width: double.infinity,
                  child: Text(
                    model.cModel,
                    style: Theme.of(context).textTheme.headline6!.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.normal,
                        color: Colors.white.withOpacity(.5),
                        letterSpacing: 1),
                  ),
                ),
              ),
              Hero(
                tag: model.cImage,
                child: Image(
                  image: AssetImage( model.cImage),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Details",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1,
                            fontSize: 16),
                      ),
                      height: 60,
                    ),
                  ),
                  Expanded(
                    child: Hero(
                      tag:model.cPrice,
                      child: Container(
                        color: Colors.white,
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  "\$12.999",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(
                                        fontSize: 20,
                                        fontWeight: FontWeight.normal,
                                      ),
                                ),
                                Text(
                                  ">",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .copyWith(
                                        fontSize: 25,
                                        fontWeight: FontWeight.normal,
                                      ),
                                ),
                              ],
                            )
                          ],
                        ),
                        height: 60,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }


  Widget _flightShuttleBuilder(
      BuildContext flightContext,
      Animation<double> animation,
      HeroFlightDirection flightDirection,
      BuildContext fromHeroContext,
      BuildContext toHeroContext,
      ) {
    return DefaultTextStyle(
      style: DefaultTextStyle.of(toHeroContext).style,
      child: toHeroContext.widget,
    );
  }
}

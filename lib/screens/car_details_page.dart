import 'package:car_rental_animation/models/class_model.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CarDetailsPage extends StatefulWidget {

  CarModel model;

  @override
  _CarDetailsPageState createState() => _CarDetailsPageState();

  CarDetailsPage({required this.model});
}

class _CarDetailsPageState extends State<CarDetailsPage> {



  Widget _flightShuttleBuilder(
      BuildContext flightContext,
      Animation<double> animation,
      HeroFlightDirection flightDirection,
      BuildContext fromHeroContext,
      BuildContext toHeroContext,
      ) {
    return DefaultTextStyle(
      style: DefaultTextStyle.of(toHeroContext).style,
      child: toHeroContext.widget,
    );
  }

  Widget build(BuildContext context) {
    final TextStyle style = Theme.of(context).textTheme.headline6!.copyWith(
          fontSize: 20,
          fontWeight: FontWeight.normal,
        );
    return Scaffold(
      backgroundColor: Color(0xFFcf7a53),

      body: Container(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              TweenAnimationBuilder(
                builder: (BuildContext context, double? value, Widget? child) {
                  return Positioned(
                      top: value,//-350 to -50
                      right: 50,
                      child:  RotatedBox(


                          quarterTurns:3,
                          child: Text("Mercedes", style:GoogleFonts.adamina(
                              fontSize: 60, color: Colors.black87, letterSpacing: 5,fontWeight: FontWeight.bold),textWidthBasis: TextWidthBasis.longestLine,),),
                  );

                },
                duration: Duration(milliseconds: 500),
                tween: Tween<double>(begin: -350,end: -50),

              ),
              Padding(
                padding: const EdgeInsets.only(left: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 150,
                    ),

                    Hero(
                      tag: widget.model.cName,
                      transitionOnUserGestures: true,
                     // flightShuttleBuilder:_flightShuttleBuilder ,
                      child: Container(

                        width: double.infinity,
                        child: Text(
                          widget.model.cName,
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                              fontSize: 25,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Hero(
                      tag: widget.model.cModel,

                      child: Container(

                        width: double.infinity,
                        child: Text(
                          widget.model.cModel,
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white.withOpacity(.5),
                              letterSpacing: 1),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Hero(
                      tag: widget.model.cImage,
                      child: Image(
                        image: AssetImage(widget.model.cImage),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 15),
                      child: TweenAnimationBuilder(
                        tween: Tween<Offset>(
                          begin: Offset(250, 0),
                          end: Offset(0, 0),
                        ),
                        duration: Duration(milliseconds: 500),
                        builder: (BuildContext context, Offset value, Widget? child) {
                          return Transform.translate(
                            offset: value,
                            child: child,
                          );
                        },
                        child: Container(
                          height: 100,
                          alignment: Alignment.center,
                          child: ListView.builder(
                            padding: EdgeInsets.zero,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding:
                                EdgeInsets.symmetric(horizontal: 2, vertical: 5),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child: Container(
                                      height: 100,
                                      color: Color(0xFF333333),
                                      alignment: Alignment.center,
                                      child: Text("Engine",style: TextStyle(color: Colors.white.withOpacity(.7)),),
                                      width: 100),
                                ),
                              );
                            },
                            itemCount: 10,
                            scrollDirection: Axis.horizontal,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 70,
                    ),
                    TweenAnimationBuilder(
                        tween: Tween<double>(begin: 0, end: 1),
                        builder: (BuildContext context, double value, Widget? child) {
                          return Transform.translate(
                              offset: Offset(0, -30 * value),
                              child: Opacity(opacity: value, child: child));
                        },
                        duration: Duration(milliseconds: 500),
                        child: Text(
                          "Instinctive Techology",
                          style: GoogleFonts.adamina(
                            fontSize: 22,
                            color: Colors.white.withOpacity(.9),
                            letterSpacing: 1,
                          ),
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    TweenAnimationBuilder(
                        tween: Tween<double>(begin: 0, end: 1),
                        builder: (BuildContext context, double value, Widget? child) {
                          return Transform.translate(
                              offset: Offset(0, -30 * value),
                              child: Opacity(opacity: value, child: child));
                        },
                        duration: Duration(milliseconds: 500),
                        child: Text(
                          "BMW’s flagship car, the 7 Series is available with petrol and diesel engine. Interestingly, for the first time in India, the new BMW 7 Series is available in a plug-in-hybrid option - the BMW 745Le xDrive - which combines benefits of an electric motor and a petrol engine. Apart from this, the 730Ld (diesel) is available in three trims – Design Pure Excellence, Design Pure Excellence Signature and the M Sport. ",
                          style: GoogleFonts.adamina(
                              fontSize: 14,
                              color: Colors.white.withOpacity(.5),
                              letterSpacing: 1),
                        )),
                  ],
                ),
              ),
              Positioned(top: 70,left: 25,
              child: InkWell(
                onTap: (){
                  Navigator.pop(context);
                },
                  child: Icon(Icons.arrow_back_outlined,color: Colors.white,size: 30,)))
            ],

          ),
        ),
      ),
      extendBody: true,
      bottomNavigationBar: Row(
        children: [
          Expanded(child: Container(
            height: 60,
          )),
          Expanded(
            child: Hero(
              tag: widget.model.cPrice,
              child: Container(
                color: Colors.white,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "\$12.999",
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                            fontSize: 25,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        Text(
                          ">",
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                            fontSize: 30,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                height: 100,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
